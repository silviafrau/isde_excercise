import datetime
import numpy as np


class Note:

    next_id = 0

    def __init__(self, note, tags):
        self.creation_date = datetime.datetime.now()
        self.tags = tags
        self.memo = note
        self.id = Note.next_id
        Note.next_id += 1

    def modify_note_content(self, new_note):
        self.memo = new_note

    def modify_note_tags(self, new_tags):
        self.tags = new_tags

    def match(self, word):
        if word.lower() in self.memo.lower() or word.lower() in [tag.lower() for tag in self.tags]:
            return True
        return False

    def __str__(self):
        return "\n  Date: {}\n  Id: {}\n  Memo: {}\n  Tags: {}".format(self.creation_date.strftime("%Y-%m-%d %H:%M:%S"), self.id, self.memo, ', '.join(self.tags))



class Notebook:
    def __init__(self, Notes=[]):
        self.notes = Notes

    def new_note(self, memo, tags=''):
        self.notes.append(Note(memo, tags))

    def modify_memo(self, note_id, memo):
        if note_id in self:
            self.notes[note_id].modify_note_content(memo)

    def modify_tag(self, note_id, tags):
        if note_id in self:
            self.notes[note_id].modify_note_tags(tags)

    def search(self, str_filter):
        return [note for note in self.notes if note.match(str_filter)]

    def get_note_by_id(self, note_id):
        for note in self.notes:
            if note.id == note_id:
                return note
        return None

