from notebook import Notebook

class Menu:

    def __init__(self):
        self.notebook = Notebook()

    def run(self):
        exit_flag = False
        while not exit_flag:
            exit_flag = self.display_menu()

    def show_all_notes(self, notes = None ):

        if notes is None:
            notes = self.notebook.notes

        print("Your notes: \n")

        for note in notes:
            print(note)

        pass

    def search_notes(self):
        search_part = input("Chiave di ricerca: ")
        notes = self.notebook.search(search_part)
        self.show_all_notes(notes)

    def add_note(self):
        memo = input("Inserisci nota: ")
        tags = input("Inserisci tag, separati da una virgola (opzionale): ").split(',')
        self.notebook.new_note(memo, tags)

    def modify_note(self):

        self.show_all_notes()

        try:
            note_id = int(input("\nInserisci ID della nota da modificare: "))
        except ValueError:
            print("Formato errato")
            return

        note = self.notebook.get_note_by_id(note_id)

        if note is None:
            print("La nota specificata non esiste.")
            return

        memo = input("Inserisci nota: ")
        note.modify_note_content(memo)

        tags = input("Inserisci tag, separati da una virgola (opzionale): ").split(',')
        note.modify_note_tags(tags)

    def display_menu(self):
        print("""

                Notebook Menu

                1. Show all Notes

                2. Search Notes

                3. Add Note

                4. Modify Note

                5. Quit 
                """)

        try:
            choice = int(input("Choice: "))
        except ValueError:
            print("Input not valid")
            return False

        if choice == 1:
            self.show_all_notes()
        elif choice == 2:
            self.search_notes()
        elif choice == 3:
            self.add_note()
        elif choice == 4:
            self.modify_note()
        elif choice == 5:
            return True

        input("\nPremi INVIO per continuare...")
        return False




