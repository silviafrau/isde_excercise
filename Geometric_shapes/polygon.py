from abc import ABC, abstractmethod


class Polygon(ABC) :
    def __init__(self,num_sides):
        self.num_sides = num_sides

    @abstractmethod
    def perimeter(self):
        pass


class Square(Polygon):
    def __init__(self, side_length):
        super().__init__(4)
        self.side_length = side_length


    def perimeter(self):
        super().perimeter()
        return self.side_length * 4


class Triangle(Polygon):
    def __init__(self,l1,l2,l3):
        super().__init__(3)
        self.l1 = l1
        self.l2 = l2
        self.l3 = l3

    def perimeter(self):
        super(Triangle, self).perimeter()
        return self.l1 + self.l2 + self.l3




